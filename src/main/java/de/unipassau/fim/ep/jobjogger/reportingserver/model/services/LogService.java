package de.unipassau.fim.ep.jobjogger.reportingserver.model.services;

import io.kubernetes.client.ApiException;
import io.kubernetes.client.apis.CoreV1Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service to handle logs from pods
 */
@Service
public class LogService {

    @Autowired
    private CoreV1Api api;

    /**
     * Return Logs from a certain pod
     *
     * @throws ApiException if error happens
     */
    public String getLogsString(String namespace, String name, Integer lines) throws ApiException {
        return api.readNamespacedPodLog(name, namespace, null, null, null, null,
                null, null, lines, null);
    }
}

package de.unipassau.fim.ep.jobjogger.reportingserver.application.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.oauth2.provider.expression.OAuth2MethodSecurityExpressionHandler;

/**
 * Enables global method security
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class Oauth2GlobalMethodConfig extends GlobalMethodSecurityConfiguration {

    /**
     * Provides a expression handler to handle Preauthorize expressions of methods
     *
     * @return the Oauth2 expression handler
     */
    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
            return new OAuth2MethodSecurityExpressionHandler();
    }
}


package de.unipassau.fim.ep.jobjogger.reportingserver.web.controllers;

import de.unipassau.fim.ep.jobjogger.reportingserver.model.services.LogService;
import io.kubernetes.client.ApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller that handles pod logging
 */
@Controller
public class LogController{

    @Autowired
    private LogService logService;

    private Logger logger = LoggerFactory.getLogger(LogController.class);

    /**
     * Get logs from the pod of a specific job
     *
     * @param username The user who owns the job
     * @param id The Job id
     * @param lines The amount of lines to be loaded
     * @return 200 OK Status and the String with the logs
     */
    @GetMapping("/users/{username}/jobs/{id}/logs")
    public ResponseEntity<String> getLogsString(@PathVariable String username, @PathVariable String id,
                                                @RequestParam(required = false, defaultValue = "0") String lines){
        String logs = "";
        Integer linesInt = Integer.valueOf(lines);

        try {
            logs = logService.getLogsString(username, id, linesInt);
        } catch (ApiException e){
            logger.error("Cannot Read Logs from pod: " + username + ", " +  id);
            logger.error(e.getMessage());
        }
        return new ResponseEntity<>(logs, HttpStatus.OK);
    }
}
package de.unipassau.fim.ep.jobjogger.reportingserver.application.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationManager;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Collections;

/**
 * Specify resource-server specific properties and configure the access rules
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    /**
     * Add the id, auth manager and token extractor properties to resource server
     *
     * @param resources configurer for the resource server
     */
    @Override
    public void configure(ResourceServerSecurityConfigurer resources){
        resources.resourceId("resource-server").authenticationManager(authenticationManagerBean())
            .tokenExtractor(new CustomTokenExtractor());
    }

    /**
     * Initiates the token service
     *
     * @return the custom build remote token service
     */
    @Bean
    public ResourceServerTokenServices tokenService() {
        return new CustomRemoteTokenService();
    }

    /**
     * Initiates the authentication manager and sets the token service
     *
     * @return the authentication manager
     */
    @Bean
    public AuthenticationManager authenticationManagerBean() {
        OAuth2AuthenticationManager authenticationManager = new OAuth2AuthenticationManager();
        authenticationManager.setTokenServices(tokenService());
        return authenticationManager;
    }

    /**
     * Cors configuration for all resource server paths
     *
     * @return the cors configuration source
     */
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Collections.singletonList("*"));
        configuration.addAllowedHeader("*");
        configuration.addAllowedMethod("*");
        configuration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    /**
     * Configures that all endpoints except the /error are protected
     *
     * @param http the http filter
     * @throws Exception when theres a problem
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                    .antMatchers("/error").permitAll()
                    .anyRequest().authenticated();
        http.cors().and().csrf().disable();
    }
}

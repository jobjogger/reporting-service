FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE=target/reporting-service.jar
COPY ${JAR_FILE} app.jar
RUN apk update && \
    apk add --update bash && \
    apk add --update git
ENTRYPOINT  java -Djava.security.egd=file:/dev/./urandom -jar /app.jar
# JobJogger Reporting Service
This repository contains the code of our JobJogger Reporting Service. 
It retrieves the logs of pods (Jobs) and offers an endpoint to read them by the Client.

## Environment Variables
| Name                 | Description                           |
|----------------------|---------------------------------------|
| AUTH_SERVER_HOST     | Hostname of the Authentication Server |
| RESOURCE_SERVER_HOST | Hostname of the Resource Server       |
